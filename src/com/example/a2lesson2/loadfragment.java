package com.example.a2lesson2;



import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.format.DateUtils;
import android.widget.TextView;

public class loadfragment extends Fragment implements LoaderManager.LoaderCallbacks<String>{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
		
		 LoaderManager lm = getActivity().getSupportLoaderManager();
		    Loader<String> loader = lm.initLoader(0, null, this);
		    if (!loader.isStarted())
		      loader.forceLoad();
		return inflater.inflate(R.layout.loadfragment, container, false);
	  }
	  
	 
	

	@Override
	public Loader<String> onCreateLoader(int loaderId, Bundle args) {
		// TODO Auto-generated method stub
		return new MyLoader(this.getActivity());
	}


	@Override
	public void onLoadFinished(Loader<String> loader, String data) {
		// TODO Auto-generated method stub
		TextView tv = (TextView) getView().findViewById(R.id.text);
	    tv.setText(data);
		
	}


	@Override
	public void onLoaderReset(Loader<String> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	  private static class MyLoader extends AsyncTaskLoader<String> {

			public MyLoader(Context context) {
				super(context);
				// TODO Auto-generated constructor stub
			}

			@Override
		    public String loadInBackground() {
		      try {
		        Thread.sleep(DateUtils.SECOND_IN_MILLIS * 5);
		      } catch (InterruptedException e) {
		      }
		      
		      return "AsyncTaskLoader Complete!";
		    }
		  }



}
