package com.example.a2lesson2;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.format.DateUtils;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<String> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


	 LoaderManager lm = getSupportLoaderManager();
	    Loader<String> loader = lm.initLoader(0, null, this);
	    if (!loader.isStarted())
	      loader.forceLoad();
	  }
	  
	  @Override
	  public Loader<String> onCreateLoader(int loaderId, Bundle args) {
	    return new MyLoader(this);
	  }
	  
	  @Override
	  public void onLoadFinished(Loader<String> loader, String data) {
	    TextView tv = (TextView) findViewById(R.id.text);
	    tv.setText(data);
	  }
	  
	  @Override
	  public void onLoaderReset(Loader<String> loader) {
	  }
	    
	  private static class MyLoader extends AsyncTaskLoader<String> {
	    public MyLoader(Context context) {
	      super(context);
	    }
	      
	    @Override
	    public String loadInBackground() {
	      try {
	        Thread.sleep(DateUtils.SECOND_IN_MILLIS * 5);
	      } catch (InterruptedException e) {
	      }
	      
	      return "AsyncTaskLoader Complete!";
	    }
	  }



}
